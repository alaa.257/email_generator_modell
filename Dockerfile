FROM python:3-alpine

LABEL author="alaa"
LABEL description="Dockerfile for Python script which generates emails"
RUN pip install tqdm
COPY . /app
WORKDIR /app
CMD ["python","/app/email_generator.py"]
