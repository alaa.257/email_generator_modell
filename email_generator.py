import json  # to read json files
import os# to access operation for get and changing directory
import sys
import random
from os import environ

from tqdm import tqdm
import hashlib


def writeTextFile(text, INDEX):
    f = open(ZIEL + '/email_%s.txt' % INDEX, 'w+')
    f.write(text)
    f.close()


def writeHashFile(text):
    f = open(ZIEL + '/00_Hash.json', 'w+')
    f.write(str(text))
    f.close()


def readJsonCordinate(fileName):
    """Read the json data."""
    with open(fileName, 'r', encoding='utf-8') as f:  # Opening the file
        data = json.load(f)  # Read the json file
    return data


def convert(x):
    x = x.replace("\t", "\\t")
    x = x.replace("\v", "\\v")
    x = x.replace("\0", "\\0")
    x = x.replace("\b", "\\b")
    x = x.replace("\f", "\\f")
    x = x.replace("\\", "\\\\")

    return x


def toJson(body, titel, artikel, INDEX):
    dict1 = {
        "data":
            {
                "subject": ""
                ,
                "body": ""
            },
        "meta":
            {
                "article": []
            }
    }
    dict1['data']['subject'] = convert(titel)
    dict1['data']['body'] = convert(body)
    dict1['meta']['article'] = [artikel]
    with open(ZIEL + "/email_%s.json" % INDEX, 'w+', encoding='utf-8') as outfile:
        json.dump(dict1, outfile, ensure_ascii=False)


def TextToHash(text):
    x = hashlib.md5(text.encode())
    return x.hexdigest()


def search(hashed_new):
    h_exist = 0
    for i in range(len(hash_dict['data'])):
        if (hashed == hash_dict['data'][i]):
            h_exist += 1  # do nothing
    return (bool(h_exist))


def addToHashDict(hashed):
    JtoL = hash_dict['data']
    JtoL.append(hashed)
    hash_dict.update({'data': JtoL})


hash_dict = {'data': []}

# Eingaben
#
#QUELLE = input("\nPfad für Wörterbücher: ")
#ZIEL = input("\nSpeicherort für erstellte Emails: ")

#os.environ["QUELLE"]= '/Users/alaaballan/desktop/Worterbuecher/Artikel_7'
#os.environ["ZIEL"]= '/Users/alaaballan/desktop/Worterbuecher/test'
#os.environ["ARTKIL_NR"]= "7"
#os.environ["ANZAHL_EMAILS"]= "20"
#os.environ["BEST_PR"]= "50"
#os.environ["ANSPRUCH_PR"]= "50"
#os.environ["KENN_PR"]= "50"
#os.environ["INDEX"]= "0"
QUELLE =os.environ.get("QUELLE")
if QUELLE is None:
    QUELLE= './Artikel_7'   
ZIEL= os.environ.get("ZIEL")
if ZIEL is None:
    ZIEL= './Email'
ARTKIL_NR=os.environ.get('ARTKIL_NR')
if ARTKIL_NR is None:
    ARTKIL_NR= 7
else:
    ARTKIL_NR=int(os.environ.get('ARTKIL_NR'))
ANZAHL_EMAILS = os.environ.get('ANZAHL_EMAILS')
if ANZAHL_EMAILS is None:
    ANZAHL_EMAILS= 50
else:
    ANZAHL_EMAILS = int(os.environ.get('ANZAHL_EMAILS'))
BEST_PR = os.environ.get('BEST_PR')
if BEST_PR is None:
    BEST_PR= 50
else:
    BEST_PR = int(os.environ.get('BEST_PR'))
ANSPRUCH_PR = os.environ.get('ANSPRUCH_PR')
if ANSPRUCH_PR is None:
    ANSPRUCH_PR= 50
else:
    ANSPRUCH_PR = int(os.environ.get('ANSPRUCH_PR'))
KENN_PR = os.environ.get('KENN_PR')
if KENN_PR is None:
    KENN_PR= 50
else:
    KENN_PR = int(os.environ.get('KENN_PR'))
INDEX = os.environ.get('INDEX')
if INDEX is None:
    INDEX= 0
else:
    INDEX = int(os.environ.get('INDEX'))

#quelle = convert(quelle)
#ziel = convert(ziel)

# Variablen : Bestätigung

BEST_PR = int((BEST_PR / 100.0) * ANZAHL_EMAILS)
best_cr = random.sample(range(int(ANZAHL_EMAILS)), BEST_PR)
best_cr.sort()

# Variablen : Anspruch

ANSPRUCH_PR = int((ANSPRUCH_PR / 100.0) * ANZAHL_EMAILS)
anspruch_cr = random.sample(range(int(ANZAHL_EMAILS)), ANSPRUCH_PR)
anspruch_cr.sort()

# Variablen : Kennung

KENN_PR = int((KENN_PR / 100.0) * ANZAHL_EMAILS)
kenn_cr = random.sample(range(int(ANZAHL_EMAILS)), KENN_PR)
kenn_cr.sort()

# Wörterbücher einlesen

betreff = readJsonCordinate(QUELLE + '/Betreff.json')
anreden = readJsonCordinate(QUELLE + '/Anreden.json')
vornamen = readJsonCordinate(QUELLE + '/Vornamen.json')
nachnamen = readJsonCordinate(QUELLE + '/Nachnamen.json')
vorhaben = readJsonCordinate(QUELLE + '/Vorhaben.json')
vorhabendetails = readJsonCordinate(QUELLE + '/Vorhabendetails.json')
kennung = readJsonCordinate(QUELLE + '/Kennung.json')
geburtsdatum = readJsonCordinate(QUELLE + '/Geburtsdatum.json')
anspruch = readJsonCordinate(QUELLE + '/Rechtlicher_Anspruch.json')
bestaetigung = readJsonCordinate(QUELLE + '/Bestaetigung.json')
abschlussworte = readJsonCordinate(QUELLE + '/Abschlussworte.json')
kennung_Nr = readJsonCordinate(QUELLE + '/Kennung_Nr.json')


# define the length of variables
len_betreff = len(betreff['betreff'])
len_anreden = len(anreden['anreden'])
len_vornamen = len(vornamen['vornamen'])
len_nachnamen = len(nachnamen['nachname'])
len_vorhaben = len(vorhaben['vorhaben'])
len_vorhabendetails = len(vorhabendetails['vorhabendetails'])
len_kennung = len(kennung['identifikation'])
len_geburtsdatum = len(geburtsdatum['geburtsdatum'])
len_anspruch = len(anspruch['rechtlicher_Anspruch'])
len_bestaetigung = len(bestaetigung['bestaetigung'])
len_abschlussworte = len(abschlussworte['abschlussworte'])
len_kennung_Nr = len(kennung_Nr['kennung_Nr'])

# Geburtsdatum , Kennungsnr sind nicht gezählt
max_emails = len_betreff * len_anreden * len_vornamen * len_nachnamen * len_vorhaben * len_vorhabendetails * len_kennung * len_anspruch * len_bestaetigung * len_abschlussworte

#Kennungsnr = [1458745, 2547, 985478, 21569, 87451, 12547, 87459, 12698, 1258478, 85236, 44745, 1258, 4555, 2125, 3225,1224, 4578]

ignored = 0


for a in tqdm(range((int(ANZAHL_EMAILS))), ncols=90):
    # Betreff
    t1 = random.randrange(len_betreff)
    p1 = betreff['betreff'][t1]
    # Anrede
    t2 = random.randrange(len_anreden)
    p2 = anreden['anreden'][t2]
    # Nachname
    t3 = random.randrange(len_nachnamen)
    if t2 in range(14, 26):
        p3 = ""
    else:
        p3 = nachnamen['nachname'][t3] + ','
        # vorhaben
    t4 = random.randrange(len_vorhaben)
    p4 = vorhaben['vorhaben'][t4]
    # vorhabendetail
    t5 = random.randrange(len_vorhabendetails)
    p5 = vorhabendetails['vorhabendetails'][t5]
    # Kennung
    if (a in kenn_cr):
        t6 = random.randrange(len_kennung)
        p6 = kennung['identifikation'][t6] + '\n'
        # geburtsdatum
        t7 = random.randrange(len_geburtsdatum)
        t7_1=random.randrange(len_kennung_Nr)
        p7 = 'Geburtsdatum: ' + geburtsdatum['geburtsdatum'][t7] + '\n' + 'Kennungsnummer: ' + str(
            kennung_Nr['kennung_Nr'][t7_1]) + '\n'
    else:
        p6 = ""
        p7 = ""
    # Anspruch

    if (a in anspruch_cr):
        t8 = random.randrange(len_anspruch)
        p8 = anspruch['rechtlicher_Anspruch'][t8] + "\n"
    else:
        p8 = ""
    # Bestätigung

    if (a in best_cr):
        t9 = random.randrange(len_bestaetigung)
        p9 = bestaetigung['bestaetigung'][t9] + "\n"
    else:
        p9 = ""
    # Abschlussworte
    t10 = random.randrange(len_abschlussworte)
    p10 = abschlussworte['abschlussworte'][t10]
    # vorname
    t11 = random.randrange(len_vornamen)
    p11 = vornamen['vornamen'][t11]
    # nachname
    t12 = random.randrange(len_nachnamen)
    p12 = nachnamen['nachname'][t12]
    email = p1 + '\n\n' + p2 + ' ' + p3 + '\n\n' + p4 + '\n' + p5 + '\n' + p6 + p7 + p8 + p9 + '\n' + p10 + '\n\n' + p11 + ' ' + p12
    hashed = TextToHash(email)
    if (len(hash_dict['data']) == 0):
        addToHashDict(hashed)
        writeTextFile(email, INDEX)
        toJson(email, p1, ARTKIL_NR, INDEX)
        INDEX += 1
    else:
        if (search(hashed)):
            ignored += 1
        else:
            addToHashDict(hashed)
            writeTextFile(email, INDEX)
            toJson(email, p1, ARTKIL_NR, INDEX)
            INDEX += 1
    writeHashFile(hash_dict)
print("**********************\n", ignored, "duplicate emails have been ignored", sep=" ")
print("**********************\n", ANZAHL_EMAILS, "emails of max", max_emails, "have been generated",
      "\n**********************", sep=" ")
#print(best_cr)
#print(anspruch_cr)
#print(kenn_cr)
#print("best+ans+kenn:  ", set(best_cr) & set(anspruch_cr) & set(kenn_cr))
#print("best+ans:  ", set(best_cr) & set(anspruch_cr))
#print("best+kenn:  ", set(best_cr) & set(kenn_cr))
#print("ans+kenn:  ", set(anspruch_cr) & set(kenn_cr))

