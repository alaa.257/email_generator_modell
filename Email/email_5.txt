Wiederruf der Einwilligung meiner persönlichen Daten nach Artikel 7 der DSGVO

Liebe Frau Becker,

hiermit wiederufe ich meine Einwillung zu Verarbeitung von personenbezogenen Daten zum Zweck des Profiling, der Direktwerbung und wissenschaftlichen oder historischen Forschungszwecken.
Der Widerruf hat keinen Einfluß auf die sonstigen Vertragsbeziehungen mit Ihnen.
Zu meiner Identifikation füge ich Ihnen folgende Daten bei:
Geburtsdatum: 13.05.1987
Kennungsnummer: 2547
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.
Bitte bestätigen sie mir schriftlich den Erhalt meiner Email

Schönen Tag

Julia Klein