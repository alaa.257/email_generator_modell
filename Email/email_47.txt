Widerruf meiner Einwilligung zu Verarbeitung

Hi Herr Schneider,

gemäß der Datenschutz-Grundverordnung (DSGVO) widerspreche ich der Verarbeitung oder Nutzung meiner personenbezogenen Daten für Direktwerbung.
Vielmehr sind davon alle weiteren Transaktionen und Prozesse betroffen, welche meine Daten nutzen.
Sollten Sie entgegen meines Wunsch handeln, so werde ich rechtliche Schritte gegen Sie einleiten und ggf. einen daraus resultierenden Schadensersatzanspruch geltend machen.

LG

Julia Neumann