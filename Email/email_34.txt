Widerruf der Einwilligungserklärung nach Art. 7 DSG-VO

Lieber Herr Schwarz,

hiermit widerspreche ich der zukünftigen Datennutzung.
Der Widerruf hat keinen Einfluß auf die sonstigen Vertragsbeziehungen mit Ihnen.
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.
Bitte bestätigen Sie mir schriftlich den Empfang dieses Schreibens

Mit freundlichen Grüßen

Dennis Koch