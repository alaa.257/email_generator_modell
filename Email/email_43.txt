Widerrufsbegehren gem. Art. 7 DSGVO

Guten Tag Herr Müller,

ich möchte meine Einwilligung zur Datenverarbeitung mit diesem Schreiben wieder rückgängig machen.
Der Widerspruch zur Datennutzung gilt für alle schriftlichen Anfragen, sowie auch für automatisierte Abfragen.
Meine Person:
Geburtsdatum: 24.06.1995
Kennungsnummer: 1224
Senden Sie mir bitte per Email eine Bestätigung,dass Sie meine Email erhalten haben

MfG

Julia Schulz