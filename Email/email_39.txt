Widerspruch gegen die Weitergabe meiner Daten

Hallo Herr Schwarz,

hiermit wiederufe ich meine Einwillung zu Verarbeitung von personenbezogenen Daten zum Zweck des Profiling, der Direktwerbung und wissenschaftlichen oder historischen Forschungszwecken.
Der Widerspruch zur Datennutzung gilt für alle schriftlichen Anfragen, sowie auch für automatisierte Abfragen.
folgende Information zur Kennung meiner Person:
Geburtsdatum: 05.06.1982
Kennungsnummer: 2547
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.

Liebe Grüße

Marcel Richter