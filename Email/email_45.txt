Widerrufsbegehren gem. Art. 7 DSGVO

Guten Abend Herr Schulz,

laut Art. 7 der Datenschutzgrundverordnung steht mir das Recht zu, die Einwilligung zur Datenverarbeitung zu widerrufen, sofern diese nicht zur Erfüllung des vorhandenen Vertrages relevant sind.
Der Widerspruch zur Datennutzung gilt für alle schriftlichen Anfragen, sowie auch für automatisierte Abfragen.
Um meine Person zu identifizieren, hänge ich Ihnen folgende Daten an:
Geburtsdatum: 05.06.1982
Kennungsnummer: 44745
Sollten Sie entgegen meines Wunsch handeln, so werde ich rechtliche Schritte gegen Sie einleiten und ggf. einen daraus resultierenden Schadensersatzanspruch geltend machen.

Viele Grüße

Eddie Becker