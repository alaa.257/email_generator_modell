Verarbeitung personenbezogener Daten Art. 7 DSGVO

Hallo Frau Hoffmann,

gemäß der Datenschutz-Grundverordnung (DSGVO) widerspreche ich der Verarbeitung oder Nutzung meiner personenbezogenen Daten für Direktwerbung.
Der Widerspruch gilt sowohl für persönliche und schriftliche Anfragen als auch für automatisierte Abrufe über das Internet.
Zur Identifikation meiner Person hänge ich folgende Daten an:
Geburtsdatum: 1.06.1951
Kennungsnummer: 4555
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.

Gruß

Sara Schäfer