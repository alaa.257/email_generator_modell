Widerspruch gemäß Artikel 21 Absatz 2 DSGVO

Hi 

hiermit wiederufe ich meine Einwillung zu Verarbeitung von personenbezogenen Daten zum Zweck des Profiling, der Direktwerbung und wissenschaftlichen oder historischen Forschungszwecken.
Der Widerruf hat keinen Einfluß auf die sonstigen Vertragsbeziehungen mit Ihnen.
Um meine Person zu identifizieren, hänge ich Ihnen folgende Daten an:
Geburtsdatum: 04.10.1945
Kennungsnummer: 4555
Bitte bestätigen Sie mir schriftlich den Empfang dieses Schreibens

Grüße

Julia Zimmermann