Verarbeitung personenbezogener Daten Art. 7 DSGVO

Guten Tag 

ich möchte meine Einwilligung zur Datenverarbeitung mit diesem Schreiben wieder rückgängig machen.
Vielmehr sind davon alle weiteren Transaktionen und Prozesse betroffen, welche meine Daten nutzen.
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.

Schönen Tag

Elena Bauer