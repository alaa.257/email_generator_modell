Widerspruch gegen die Weitergabe meiner Daten

Guten Abend Herr Müller,

hiermit widerspreche ich der Verarbeitung meiner bei Ihnen gespeicherten personenbezogenen Daten, die aufgrund der DSGVO erfolgt.
Der Widerspruch zur Datennutzung gilt für alle schriftlichen Anfragen, sowie auch für automatisierte Abfragen.
Wenn meine Email bei Ihenen eingegangen ist, bestätigen Sie dies schriftlich

VG

Christoph Hoffmann