Verarbeitung personenbezogener Daten Art. 7 DSGVO

Guten Morgen 

hiermit wiederufe ich meine Einwillung zu Verarbeitung von personenbezogenen Daten zum Zweck des Profiling, der Direktwerbung und wissenschaftlichen oder historischen Forschungszwecken.
Wenn Sie anderen Firmen auch meine Daten erteilt haben, informieren Sie Sie bzgl. meiner Bitte.
Sollten Sie meinem Löschungsersuchen nicht nachkommen, fordere ich Sie auf, Ihre Entscheidung mir gegenüber unter Angabe der gesetzlichen Grundlage unverzüglich zu begründen.
Wenn meine Email bei Ihenen eingegangen ist, bestätigen Sie dies schriftlich

Mit freundlichem Gruß

Sara Schulz