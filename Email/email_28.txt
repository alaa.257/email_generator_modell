Widerruf der Einwilligungserklärung nach Art. 7 DSG-VO

Guten Tag Herr Schneider,

mit diesem Schreiben Widerspreche ich der Verarbeitung meiner personenbezogenen Daten durch die Loveshop GmbH.
Die Einwilligung meinerseits ist auf freiwilliger Basis geschehen, allerdings möchte ich diese nun rückgängig machen.
Falls dies nicht geschieht, werde ich einen Anwalt kontaktieren, der dieses Problem aufgreift und löst.
Bitte bestätigen Sie mir schriftlich den Empfang dieses Schreibens

MfG

Dominik Fischer