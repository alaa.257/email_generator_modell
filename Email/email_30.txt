Widerruf der Einwilligungserklärung nach Art. 7 DSG-VO

Hallo Frau Müller,

hiermit widerspreche ich der Verarbeitung meiner bei Ihnen gespeicherten personenbezogenen Daten, die aufgrund der DSGVO erfolgt.
Wenn Sie anderen Firmen auch meine Daten erteilt haben, informieren Sie Sie bzgl. meiner Bitte.
Ich wäre Ihnen dankbar, wenn Sie den Eingang dieses Schreibens schriftlich bestätigen

Gruß

Florian Meyer